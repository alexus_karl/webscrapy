import requests
from bs4 import BeautifulSoup
import pandas as pd

target_products = [
    'https://www.ebay.com.au/itm/256076464296?hash=item3b9f58c0a8:g:eXsAAOSw~xpkYh42',
    'https://www.ebay.com.au/itm/285356985448?hash=item42709a0c68:g:ElsAAOSw5opkrHnx',
    'https://www.ebay.com.au/itm/285358428125?hash=item4270b00fdd:g:SykAAOSwzKpkqnTl'
]

data = []

for product_url in target_products:
    # Send a GET request to fetch the page HTML
    response = requests.get(product_url)
    if response.status_code == 200:
        page_html = response.text

        # Extract the full-size image links from the page HTML
        soup = BeautifulSoup(page_html, 'html.parser')
        image_links = []
        image_elements = soup.find_all('img')
        for img in image_elements:
            if img.has_attr('src'):
                image_links.append(img['src'])

        # Extract the category tree
        category_tree = soup.find('span', {'itemprop': 'title'})
        if category_tree:
            category_tree = category_tree.text.strip()

        # Extract the product title
        product_title = soup.find('h1', {'class': 'x-item-title__mainTitle'})
        if product_title:
            product_title = product_title.text.strip()

        # Extract vehicle compatibility
        vehicle_compatibility = []
        compatibility_table = soup.find('table', {'class': 'ux-table-section--html-table'})
        if compatibility_table:
            rows = compatibility_table.find_all('tr')
            for row in rows:
                cells = row.find_all('td')
                if len(cells) == 7:
                    fitment_position = cells[0].text.strip()
                    comment = cells[0].find('span', {'class': 'ux-textspans'}).text.strip()
                    make = cells[1].text.strip()
                    model = cells[2].text.strip()
                    year = cells[3].text.strip()
                    submodel = cells[4].text.strip()
                    variant = cells[5].text.strip()
                    engine = cells[6].text.strip()
                    vehicle_compatibility.append({
                        'Fitment Position': fitment_position,
                        'Comment': comment,
                        'Make': make,
                        'Model': model,
                        'Year': year,
                        'Submodel': submodel,
                        'Variant': variant,
                        'Engine': engine
                    })

        # Extract item specifics
        item_specifics = []
        specifics_section = soup.find('div', {'data-testid': 'ux-layout-section-module-evo'})
        if specifics_section:
            spec_rows = specifics_section.find_all('div', {'class': 'ux-layout-section-evo__row'})
            for row in spec_rows:
                spec_name = row.find('div', {'class': 'ux-labels-values__labels-content'}).text.strip()
                spec_value = row.find('div', {'class': 'ux-labels-values__values-content'}).text.strip()
                item_specifics.append({
                    'Name': spec_name,
                    'Value': spec_value
                })

        # Extract description
        description = soup.find('section', {'id': 'content1'})
        if description:
            description = description.text.strip()

        # Extract seller information
        seller_info = soup.find('div', {'data-testid': 'ux-seller-section'})
        if seller_info:
            seller_info = seller_info.text.strip()

        # Extract product price and shipping options
        price = soup.find('div', {'class': 'x-price-primary'})
        if price:
            price = price.text.strip()

        shipping_options = soup.find('div', {'class': 'ux-labels-values__values-content'})
        if shipping_options:
            shipping_options = shipping_options.text.strip()

        returns_info = soup.find('div', {'class': 'ux-labels-values--returns'})
        if returns_info:
            returns_info = returns_info.text.strip()

        payments_info = soup.find('div', {'class': 'ux-labels-values--payments'})
        if payments_info:
            payment_methods = payments_info.find_all('span', {'class': 'ux-textspans'})
            payment_methods = [method.get('title') for method in payment_methods]

        afterpay_info = soup.find('div', {'class': 'ux-labels-values--afterpay_au'})
        if afterpay_info:
            afterpay_text = afterpay_info.text.strip()

        # Add the data to the list
        data.append({
            'Page HTML': page_html,
            'Page URL': product_url,
            'Image Links': image_links,
            'Category Tree': category_tree,
            'Product Title': product_title,
            'Vehicle Compatibility': vehicle_compatibility,
            'Item Specifics': item_specifics,
            'Description': description,
            'Seller Information': seller_info,
            'Price': price,
            'Shipping Options': shipping_options,
            'Returns Information': returns_info,
            'Payment Methods': payment_methods,
            'Afterpay Information': afterpay_text
        })

# Create a DataFrame from the data list
df = pd.DataFrame(data)

# Export the DataFrame to an Excel file
df.to_excel('crawler_results.xlsx', index=False)